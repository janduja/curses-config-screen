## About
This curses program opens a screen for a configuration by select boxes and text boxes.

![cfselector preview](http://janduja.com/cconfscreen/cconfscreen.gif "cconfigscreen preview")

The widget structures are described in the header source file:
```c
typedef struct widget
{
	int type; // 0 selectbox, 1 textbox
	void* element; // pointer to textbox or selectbox
} widget;

typedef struct selectbox
{
	wchar_t* label;
	wchar_t** values;
	int size;
	int preselected;
	int selected;
} selectbox;

typedef struct textbox
{
	int cursor_position;
	wchar_t* label;
	wchar_t* init_text;
	wchar_t* selected_text;
} textbox;
```

The screen is opened by function:
```c
/*
 This function opens a screen for configuration of a list of selectbox and textbox.
 'fields' must be an array of pointers to widget structures terminated by a NULL.
 The selected_text field for a textbox should be NULL on input: it is set and
 allocated in this function, the caller should free it.
 The 'validate' function, if not NULL, performs a custom validation for the widgets
 and if its result is not NULL it will display the returned error string into the configuration
 screen upon confirmation.
 It returns 0 if the configuration is confirmed, 1 if it is discarded.
*/
int cconfscreen(WINDOW* w, wchar_t* title, widget** fields, wchar_t* validate(widget**));
```

A test program is provided and it shows an example of how to fill the structures, call the function and provide a custom validator.

Source code is standard C99 and should be compilable in any POSIX environment. It was tested on Linux, adaptation to BSD and other systems should be straightforward.

## Test Executable
Run ```make``` to generate the executable ```test```, then call it from the command line to show its features.

## License
[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)
