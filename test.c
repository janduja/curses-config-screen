/*
 Copyright (c) 2018 Raffaele Arecchi
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdlib.h>
#include <ctype.h>
#include <locale.h>
#include <ncursesw/curses.h>
#include "cconfscreen.h"

static wchar_t* validation(widget** fields)
{
	textbox* agetxt = (textbox*) (*(fields+1))->element;
	wchar_t* t = agetxt->selected_text;
	while (*t != '\0')
	{
		char* pmb = (char*)malloc(sizeof(char));
		wctomb(pmb, *t);
		if (!isdigit(*pmb))
			return L"***The age entered is not a valid number***";
		free(pmb);
		t++;
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	setlocale(LC_ALL,"");
	WINDOW* win = initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

 	selectbox* sel1 = malloc(sizeof(selectbox));
 	wchar_t* yesno[2] = {L"Yes", L"No"};
 	sel1->values = &*yesno;
	sel1->label = L"Are you happy? ";
	sel1->preselected = 0;
	sel1->size = 2;
 	widget w1;
 	w1.type = 0;
 	w1.element = sel1;

 	selectbox* sel2 = malloc(sizeof(selectbox));
 	wchar_t* feeling[4] = {L"Happy", L"Depressed", L"Not bad", L"Mind you business"};
 	sel2->values = feeling;
	sel2->label = L"Choose your mood: ";
	sel2->preselected = 0;
	sel2->size = 4;
 	widget w3;
 	w3.type = 0;
 	w3.element = sel2;
 	
 	textbox* txt1 = malloc(sizeof(textbox));
 	txt1->init_text = L"24";
 	txt1->selected_text = NULL;
	txt1->label = L"Input your age: ";
 	widget w2;
 	w2.type = 1;
 	w2.element = txt1;
 	
 	textbox* txt2 = malloc(sizeof(textbox));
 	txt2->init_text = L"";
	txt2->label = L"Favorite team: ";
 	widget w4;
 	w4.type = 1;
 	w4.element = txt2;
 	
 	widget* w[5] = { &w1, &w2, &w3, &w4, NULL };
	
	wchar_t* title = L"Configuration settings";
	int result = cconfscreen(win, title, w, validation); 	

	free(sel1);
	free(sel2);
	free(txt1);
	free(txt2);

	endwin();
	return 0;
}
