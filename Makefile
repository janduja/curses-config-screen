CC=c99
CFLAGS=-g -std=c99 -DNCURSES_WIDECHAR=1
PREFIX=/usr/local

all: test

cconfscreen.o: cconfscreen.c cconfscreen.h
	$(CC) $(CFLAGS) -c cconfscreen.c
test.o: cconfscreen.h test.c
	$(CC) $(CFLAGS) -c test.c

test: cconfscreen.o test.o
	$(CC) $(CFLAGS) cconfscreen.o test.o -o test -lncursesw

clean:
	rm -f *.o test
