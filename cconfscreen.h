/*
 Copyright (c) 2018 Raffaele Arecchi <raffaele@arecchi.eu>
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <curses.h>

#define SELECTBOX_P 0
#define TEXTBOX_P 1

typedef struct widget
{
	int type; // 0 selectbox, 1 textbox
	void* element; // pointer to textbox or selectbox
} widget;

typedef struct selectbox
{
	wchar_t* label;
	wchar_t** values;
	int size;
	int preselected;
	int selected;
} selectbox;

typedef struct textbox
{
	int cursor_position;
	wchar_t* label;
	wchar_t* init_text;
	wchar_t* selected_text;
} textbox;

/*
 This function opens a screen for configuration of a list of selectbox and textbox.
 'fields' must be an array of pointers to widget structures terminated by a NULL.
 The selected_text field for a textbox should be NULL on input: it is set and
 allocated in this function, the caller should free it.
 The 'validate' function, if not NULL, performs a custom validation for the widgets
 and if its result is not NULL it will display the returned error string into the configuration
 screen upon confirmation.
 It returns 0 if the configuration is confirmed, 1 if it is discarded.
*/
int cconfscreen(WINDOW* w, wchar_t* title, widget** fields, wchar_t* validate(widget**));