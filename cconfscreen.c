/*
 Copyright (c) 2018 Raffaele Arecchi <raffaele@arecchi.eu>
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>
#include <ctype.h>
#include <ncursesw/ncurses.h>
#include <string.h>
#include "cconfscreen.h"

#define TAB_SIZE 4
#define TEXT_SIZE 128

static int len_to_NULL(void** x)
{
	int result;
	while (*x != NULL)
	{
		result++;
		x++;
	}
	return result;
}

static void add_widec(wchar_t c, int attr)
{
	cchar_t* ch = malloc(sizeof(cchar_t));
	ch->attr = attr;
	ch->chars[0] = c;
	ch->chars[1] = '\0';
	add_wch(ch);
	free(ch);
}

static void print_widget(WINDOW* win, widget* w, int focused)
{
	int max = getmaxx(win);
	if (w->type == SELECTBOX_P)
	{
		selectbox* s = (selectbox*) w->element;
		wchar_t* l = s->label;
		while (*l != '\0')
		{
			add_widec(*l, A_NORMAL);
			l++;
		}
		add_widec(L'<', focused ? A_REVERSE : A_NORMAL);
		int selected = s->selected;
		wchar_t* sel = *(s->values + selected);
		for (int i=0; i< wcslen(sel); i++)
			add_widec( *(sel+i), A_NORMAL);
		add_widec(L'>', focused ? A_REVERSE : A_NORMAL);
	}
	if (w->type == TEXTBOX_P)
	{
		textbox* t = (textbox*) w->element;
		wchar_t* l = t->label;
		while (*l != '\0')
		{
			add_widec(*l, A_NORMAL);
			l++;
		}
		wchar_t* f = t->selected_text;
		while (*f != '\0')
		{
			if (*f == '\t')
			{
				for (int i=0; i<TAB_SIZE; i++)
					add_widec(' ', A_NORMAL);
			}
			else
				add_widec(*f, A_NORMAL);
			f++;
		}
	}
}

static void select_widget(WINDOW* win, widget** widgets, int old_pos, int new_pos)
{
	widget* w_old = *(widgets+old_pos);
	move(3+old_pos,0);
	clrtoeol();
	print_widget(win, w_old, 0);
	widget* w_new = *(widgets+new_pos);
	move(3+new_pos,0);
	print_widget(win, w_new, 1);
	if (w_new->type == SELECTBOX_P)
		curs_set(0);
	else if (w_new->type == TEXTBOX_P)
	{
		textbox* t = (textbox*) w_new->element;
		int offset_cursor = 0;
		for (int i=0; i < t->cursor_position; i++)
			if (*(t->selected_text + i) == '\t')
				offset_cursor = offset_cursor + TAB_SIZE;
			else
				offset_cursor++;
		move(3+new_pos, wcslen(t->label) + offset_cursor);
		curs_set(1);
	}
}

int cconfscreen(WINDOW* win, wchar_t* title, widget** fields, wchar_t* validate(widget**))
{
	int maxx, maxy;
	getmaxyx(win, maxy, maxx);
	clear();
	move(0, 0);
	attron(A_REVERSE);
	for (int i=0; i < maxx; i++)
		addch(' ' | A_REVERSE);
	move(0, 0);
	printw("[F1] discard, [F2] done, [Arrows] select/change/edit"); 
	attroff(A_REVERSE);

	move(1, 0);
	size_t title_len = wcslen(title);
	for(int i=0; i < wcslen(title); i++)
		add_widec(*(title+i), A_NORMAL);

	int selected_widget = 0;
	int num_widgets = len_to_NULL((void**) fields);
	
	for (int i=0; i<num_widgets; i++)
	{
		if ((*(fields+i))->type == TEXTBOX_P)
		{ // initialize text box
			textbox* t = (textbox*) (*(fields+i))->element;
			int leninit = wcslen(t->init_text);
			wchar_t* field = malloc(leninit * sizeof(wchar_t)+1);
			for (int j=0; j<leninit; j++)
				*(field+j) = *((t->init_text)+j);
			*(field+leninit) = '\0';
			t->selected_text = field;
		}
		if ((*(fields+i))->type == SELECTBOX_P)
		{
		 	selectbox* s = (selectbox*) (*(fields+i))->element;
		 	s->selected = s->preselected;
		}
		move(3+i,0);
		print_widget(win, *(fields+i), 0);
	}
	select_widget(win, fields, 0, 0);
	refresh();

	while (true)
	{
		wint_t ch;
		int gch = get_wch(&ch);
		if (gch == ERR || ch == KEY_F(1))
			return 1;
		// clear feedbak line
		move(2,0);
		clrtoeol();
		if (ch == KEY_F(2))
		{
			wchar_t* v = validate(fields);
			if (v == NULL)
				return 0;
			move(2,0);
			clrtoeol();
			while (*v != '\0')
			{
				add_widec(*v, A_NORMAL);
				v++;
			}
			refresh();	
			continue;
		}
		widget* w = *(fields+selected_widget);
		if (ch == KEY_DOWN || ch == '\n' || ch == KEY_ENTER)
		{
			if (selected_widget < num_widgets-1)
			{
				selected_widget++;
				select_widget(win, fields, selected_widget-1, selected_widget);
				refresh();
			}
			continue;
		}
		if (ch == KEY_UP)
		{
			if (selected_widget > 0)
			{
				selected_widget--;
				select_widget(win, fields, selected_widget+1, selected_widget);
				refresh();
			}
			continue;
		}
		if (ch == KEY_RIGHT)
		{
			if (w->type == SELECTBOX_P)
			{
				selectbox* s = (selectbox*) w->element;
				if (s->selected + 1 == s->size)
					s->selected = 0;
				else
					s->selected = s->selected + 1;
			}
			if (w->type == TEXTBOX_P)
			{
				textbox* t = (textbox*) w->element;
				int len = wcslen(t->selected_text);
				if (t->cursor_position < len)
					t->cursor_position = t->cursor_position+1;
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
		if (ch == KEY_LEFT)
		{
			if (w->type == SELECTBOX_P)
			{
				selectbox* s = (selectbox*) w->element;
				if (s->selected == 0)
					s->selected = s->size-1;
				else
					s->selected = s->selected - 1;
			}
			if (w->type == TEXTBOX_P)
			{
				textbox* t = (textbox*) w->element;
				int len = wcslen(t->selected_text);
				if (t->cursor_position > 0)
					t->cursor_position = t->cursor_position+-1;
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
		if (ch == KEY_HOME)
		{
			if (w->type == TEXTBOX_P)
			{
				textbox* t = (textbox*) w->element;
				t->cursor_position = 0;
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
        if (ch == KEY_END)
		{
			if (w->type == TEXTBOX_P)
			{
				textbox* t = (textbox*) w->element;
				int len = wcslen(t->selected_text);
				t->cursor_position = len;
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
		if (ch == KEY_BACKSPACE && w->type == TEXTBOX_P)
		{
			textbox* t = (textbox*) w->element;
			if (t->cursor_position > 0)
			{
				int len = wcslen(t->selected_text);
				for (int i=t->cursor_position-1; i<len; i++)
					*(t->selected_text + i) = *(t->selected_text + i + 1);
				size_t newsize = (len)*sizeof(wchar_t);
				t->selected_text = realloc(t->selected_text, newsize);
				t->cursor_position = t->cursor_position - 1;
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
		if (ch == KEY_DC && w->type == TEXTBOX_P)
		{
			textbox* t = (textbox*) w->element;
			int len = wcslen(t->selected_text);
			if (t->cursor_position < len)
			{
				for (int i=t->cursor_position; i<len; i++)
					*(t->selected_text + i) = *(t->selected_text + i + 1);
				size_t newsize = (len)*sizeof(wchar_t);
				t->selected_text = realloc(t->selected_text, newsize);
			}
			select_widget(win, fields, selected_widget, selected_widget);
			refresh();
			continue;
		}
		// insert the character only if textbox
		if (w->type == TEXTBOX_P)
		{
			textbox* t = (textbox*) w->element;
			int len = wcslen(t->selected_text);
			size_t newsize = (len+2)*sizeof(wchar_t);
			t->selected_text = realloc(t->selected_text, newsize);
			for (int i=len+1; i>=t->cursor_position; i--)
				*(t->selected_text + i) = *(t->selected_text + i - 1);
			*(t->selected_text+t->cursor_position) = ch;
			t->cursor_position = t->cursor_position + 1;
		}
		select_widget(win, fields, selected_widget, selected_widget);
		refresh();
	}

	return 0;
}